var sudoku = require('sudoku'),
    express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    path = require('path'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    morgan = require('morgan');


app.use(morgan());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({type:'application/json'}));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.get('/api/puzzle', function (req, res, next) {
  var puzzle = sudoku.makepuzzle();
  var solved = sudoku.solvepuzzle(puzzle);
  var json = JSON.stringify({puzzle: puzzle, solved: solved});
  res.header({'Content-Type': 'application/json'});
  res.send(json);
});

app.listen(port, function () {
  console.log('App is listening at http://localhost:%d', port)
});