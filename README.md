## Sudoku Puzzle back End

This is the back end for my Sudoku game. This is the server/API portion of the site. It only exposes one endpoint, which returns a puzzle and it's solution as JSON. The front end code is located [here](https://bitbucket.org/llague/sudokupuzzle). A running version of the front end can be seen [here](http://sudokufuntest.herokuapp.com/), and the JSON endpoint is [here](http://nameless-woodland-1994.herokuapp.com/api/puzzle).

The server is written in JavaScript for Node.js. To run locally, you'll need to have Node installed. I am using v0.10.28.

To build the project, you'll need to install the Node modules, then run the server.
1. Run `npm install` (assuming you have Node installed. I'm using v0.10.28)
2. Run `npm start`. This will run the server.js file.
